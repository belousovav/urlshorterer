Rails.application.routes.draw do
  resources :urls, only: [:create, :show], controller: 'short_urls' do
    resource :stats, only: [:show]
  end

  root 'short_urls#index'
end
