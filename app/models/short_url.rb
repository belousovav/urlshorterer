require 'short_url_coder'

class ShortUrl < ApplicationRecord
  validates :url, presence: true

  def self.find_by_short_url(short_url)
    id = ShortUrlCoder.decode(short_url)
    find(id)
  end

  def short_url
    return unless id

    ShortUrlCoder.encode(id)
  end
end
