class StatsController < ApplicationController
  def show
    short_url = ShortUrl.find_by_short_url(params[:url_id])
    render locals: { short_url: short_url }
  end
end
