class ShortUrlsController < ApplicationController
  def index
    render :new, locals: { short_url: ShortUrl.new }
  end

  def show
    short_url = ShortUrl.find_by_short_url(params[:id])
    short_url.increment(:counter)
    short_url.update!(visitor_ip: request.remote_ip, visited_at: Time.current)
    redirect_to short_url.url
  end

  def create
    short_url = ShortUrl.new(short_url_params)

    if short_url.save
      render :show, status: :created, locals: { short_url: short_url }
    else
      render json: { errors: short_url.errors, messages: short_url.errors.full_messages }, status: :unprocessable_entity
    end
  end

  private

  def short_url_params
    params.require(:short_url).permit(:url)
  end
end
