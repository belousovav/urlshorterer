require 'short_url_coder'

RSpec.describe ShortUrlCoder do
  describe '.encode' do
    it 'convert number to short string' do
      expect(described_class.encode(61)).to eq 'Mp'
    end
  end

  describe '.decode' do
    it 'convert short string to id' do
      expect(described_class.decode('Mp')).to eq 61
    end

    it 'return nil for unknown dict' do
      expect(described_class.decode('1')).to eq nil
    end
  end
end

