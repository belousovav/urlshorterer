require "rails_helper"

RSpec.describe StatsController, type: :routing do
  describe "routing" do
    it "routes to #show" do
      expect(get: "/urls/1/stats").to route_to("stats#show", url_id: "1")
    end
  end
end
