require 'rails_helper'

RSpec.describe ShortUrl, type: :model do
  describe 'Validations' do
    it 'valid with valid attrs' do
      expect(described_class.new(url: 'some.url')).to be_valid
    end

    it 'is not valid without url' do
      expect(described_class.new(url: nil)).not_to be_valid
    end
  end

  describe '.find_by_short_url' do
    it 'find by short url' do
      url = ShortUrl.create!(url: 'valid.url')
      expect(ShortUrl.find_by_short_url(url.short_url)).to eq url
    end
  end

  describe '#short_url' do
    it 'return short url' do
      expect(described_class.new(id: 1).short_url).to eq 'M'
    end
  end
end
