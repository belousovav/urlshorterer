# frozen_string_literal: true

# Convert id to short string and vise verse
module ShortUrlCoder
  DICT = 'DMR8cy7QYmhSpPGN9kjzTFbwgrxZ6t2vBnJ3Kq5XCfHLW4sVd'
  BASE = DICT.size

  module_function

  def encode(num)
    str = String.new
    while num.positive?
      digit = DICT[num % BASE]
      str = str.insert(0, digit)
      num /= BASE
    end
    str
  end

  def decode(str)
    str.each_char.reduce(0) do |acc, digit|
      val = DICT.index(digit)
      break unless val

      val + BASE * acc
    end
  end
end