class CreateShortUrls < ActiveRecord::Migration[6.1]
  def change
    create_table :short_urls do |t|
      t.string :url, null: false
      t.integer :counter, null: false, default: 0
      t.string :visitor_ip
      t.datetime :visited_at

      t.timestamps
    end
  end
end
